<?php

    class Animal
    {
        public $name;
        public $legs;
        public $cold_blooded;

        //use 'constract'
        public function __construct($name)
        {
            $this->name = $name;
            $this->legs = 2;
            $this->cold_blooded = 'false';
        }

        //use set and get

        //to give name
        // public function set_name($name)
        // {
        //     $this->name = $name;
        // }
        // public function get_name()
        // {
        //     return $this->name;
        // }

        // //set legs
        // public function set_legs()
        // {
        //     $this->legs = 2;
        // }
        // public function get_legs()
        // {
        //     return $this->legs;
        // }

        // //cold blooded  
        // public function set_cold_blooded()
        // {
        //     $this->cold_blooded = 'false';
        // }
        // public function get_cold_blooded()
        // {
        //     return $this->cold_blooded;
        // }

        
    }


?>