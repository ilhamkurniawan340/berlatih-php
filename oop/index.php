<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal('shaun');
$kodok = new Frog('buduk');
$sungokong = new Ape('kera sakti');

// $sheep->set_name('shaun');
// $sheep->set_legs();
// $sheep->set_cold_blooded();

echo $sheep->name . '<br>'; // "shaun"  
echo $sheep->legs . '<br>'; // 2
echo $sheep->cold_blooded; // false

echo "<br>";echo "<br>";

// $sungokong->set_name('kera sakti');
echo $sungokong->name . '<br>';
// $sungokong->set_legs();
echo $sungokong->legs . '<br>';
// $sungokong->set_cold_blooded();
echo $sungokong->cold_blooded . '<br>';
$sungokong->yell();

echo "<br>";echo "<br>";

// $kodok->set_name('buduk');
echo $kodok->name . '<br>';
// $kodok->set_cold_blooded();
echo $kodok->cold_blooded . '<br>';
$kodok->jump();


?>