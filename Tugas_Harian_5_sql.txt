query 

=================================================== Jawaban No 1. ============================================================

create database myshop;

=================================================== Jawaban No 2. ============================================================

use myshop; 

MariaDB [myshop]> create table categories(
    -> id int auto_increment,
    -> name varchar(255),
    -> primary key (id)
    -> );

MariaDB [myshop]> describe categories
    -> ;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.022 sec)

MariaDB [myshop]> create table user(
    -> id int auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> category_id int,
    -> PRIMARY KEY (id),
    -> FOREIGN KEY (category_id) REFERENCES categories(id)
    -> );
Query OK, 0 rows affected (0.250 sec)

MariaDB [myshop]> describe user;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| email       | varchar(255) | YES  |     | NULL    |                |
| password    | varchar(255) | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
5 rows in set (0.026 sec)

MariaDB [myshop]> create table items(
    -> id int auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> PRIMARY KEY (id),
    -> FOREIGN KEY (category_id) REFERENCES categories(id)
    -> );
Query OK, 0 rows affected (0.240 sec)

MariaDB [myshop]> describe items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.070 sec)

MariaDB [myshop]>

=================================================== Jawaban No 3. ============================================================

MariaDB [myshop]> INSERT INTO categories (name)
    -> VALUES ("gadget");
Query OK, 1 row affected (0.074 sec)

MariaDB [myshop]> INSERT INTO categories (name)
    -> VALUES ("cloth");
Query OK, 1 row affected (0.056 sec)

MariaDB [myshop]> select * FROM categories;
+----+--------+
| id | name   |
+----+--------+
|  1 | gadget |
|  2 | cloth  |
+----+--------+
2 rows in set (0.000 sec)

MariaDB [myshop]> INSERT INTO categories (name)
    -> VALUES ("men");
Query OK, 1 row affected (0.054 sec)

MariaDB [myshop]> INSERT INTO categories (name)
    -> VALUES ("women"),
    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near '' at line 2
MariaDB [myshop]> INSERT INTO categories (name)
    -> VALUES ("women");
Query OK, 1 row affected (0.098 sec)

MariaDB [myshop]> INSERT INTO categories (name)
    -> VALUES ("branded");
Query OK, 1 row affected (0.053 sec)

MariaDB [myshop]> select * FROM categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.000 sec)

MariaDB [myshop]> INSERT INTO user (name, email, password)
    -> VALUES ("John Doe", "john@doe.com", "john123");
Query OK, 1 row affected (0.081 sec)

MariaDB [myshop]> INSERT INTO user (name, email, password)
    -> VALUES ("Jane Doe", "jane@doe.com", "jenita123");
Query OK, 1 row affected (0.066 sec)

MariaDB [myshop]> select * FROM user;
+----+----------+--------------+-----------+-------------+
| id | name     | email        | password  | category_id |
+----+----------+--------------+-----------+-------------+
|  1 | John Doe | john@doe.com | john123   |        NULL |
|  2 | Jane Doe | jane@doe.com | jenita123 |        NULL |
+----+----------+--------------+-----------+-------------+
2 rows in set (0.000 sec)

MariaDB [myshop]> INSERT INTO items (name, description, price, stock, category_id)
    -> VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
Query OK, 1 row affected (0.055 sec)

MariaDB [myshop]> select * FROM items;
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | price   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang | 4000000 |   100 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+
1 row in set (0.000 sec)

MariaDB [myshop]> INSERT INTO items (name, description, price, stock, category_id)
    -> VALUES ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2);
Query OK, 1 row affected (0.054 sec)

MariaDB [myshop]> INSERT INTO items (name, description, price, stock, category_id)
    -> VALUES ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
Query OK, 1 row affected (0.053 sec)

MariaDB [myshop]> select * FROM items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)

=================================================== Jawaban No 4. ============================================================
		      ============================= Jawaban No A. ==============================

MariaDB [myshop]> SELECT id, name, email FROM user;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.001 sec)

=================================================== Jawaban No 4. ============================================================
		      ============================= Jawaban No B. ==============================

MariaDB [myshop]> SELECT * FROM items WHERE price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.001 sec)

MariaDB [myshop]> SELECT * FROM items WHERE name LIKE '%sang%';
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | price   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang | 4000000 |   100 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+
1 row in set (0.000 sec)

MariaDB [myshop]>


=================================================== Jawaban No 4. ============================================================
		      ============================= Jawaban No C. ==============================

MariaDB [myshop]> SELECT items.id, items.name, items.description, items.price, items.stock, items.category_id,
    -> categories.name FROM items JOIN categories ON items.category_id = categories.id;
+----+-------------+-----------------------------------+---------+-------+-------------+--------+
| id | name        | description                       | price   | stock | category_id | name   |
+----+-------------+-----------------------------------+---------+-------+-------------+--------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 | gadget |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth  |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
+----+-------------+-----------------------------------+---------+-------+-------------+--------+
3 rows in set (0.000 sec)

MariaDB [myshop]>


=================================================== Jawaban No 5. ============================================================
		
MariaDB [myshop]> update items set price=2500000 WHERE name="Sumsang b50";
Query OK, 1 row affected (0.056 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)






